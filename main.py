from pyb import Accel, Pin
from ws2812 import WS2812
import utime
from machine import rng

DIRECTION_SENS = 0.7

SENSIVITY = 0.3

UPDATES = list()
TARGET_TIME = 20
ACC = Accel()

STATE_UNKNOWN = 0
STATE_FRONT = 1
STATE_BACK = 2
STATE_LEFT = 3
STATE_RIGHT = 4
STATE_TOP = 5
STATE_BOTTOM = 6
STATE_MOVING = 7
STATE_WRONG = 8
STATE_WIN = 9

STRSTATE = {
    STATE_UNKNOWN: "unknown",
    STATE_FRONT: "front",
    STATE_BACK: "back",
    STATE_LEFT: "left",
    STATE_RIGHT: "right",
    STATE_TOP: "top",
    STATE_BOTTOM: "bottom",
    STATE_MOVING: "moving",
    STATE_WRONG: "wrong",
    STATE_WIN: "win"
}
# Верх 5-21
# Право 0-4 50-54
# Лево 22-32
# Низ 33-49
# Зад 55-70
# Итого 70

# Количество светодиодов
LEDS_LEN = 71
# Соответвие номеров светодиодов граням
LEDS_TOP = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19, 20, 21]
LEDS_RIGHT = [0, 1, 2, 3, 4, 50, 51, 52, 53, 54]
LEDS_LEFT = [22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32]
LEDS_BOTTOM = [33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49]
LEDS_BACK = [55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70]

# Количество светодидов в каждой ячейки програсс бара
LEDS_TOP_PROG = [4, 4, 4, 4]
LEDS_RIGHT_PROG = [5, 5]
LEDS_LEFT_PROG = [5, 6]
LEDS_BOTTOM_PROG = [4, 4, 4, 5]
LEDS_BACK_PROG = [4, 4, 4, 4]

# соответсвие граней состояниям
LEDSTATE = {
    STATE_UNKNOWN: None,
    STATE_FRONT: None,
    STATE_BACK: LEDS_BACK,
    STATE_LEFT: LEDS_LEFT,
    STATE_RIGHT: LEDS_RIGHT,
    STATE_TOP: LEDS_TOP,
    STATE_BOTTOM: LEDS_BOTTOM,
    STATE_MOVING: None,
    STATE_WRONG: None,
    STATE_WIN: None

}
# Соответсия  ячеек програсса стояниям(должно совпадасть с предидущим)
LEDSTATE_PROG = {
    STATE_UNKNOWN: None,
    STATE_FRONT: None,
    STATE_BACK: LEDS_BACK_PROG,
    STATE_LEFT: LEDS_LEFT_PROG,
    STATE_RIGHT: LEDS_RIGHT_PROG,
    STATE_TOP: LEDS_TOP_PROG,
    STATE_BOTTOM: LEDS_BOTTOM_PROG,
    STATE_MOVING: None,
    STATE_WRONG: None,
    STATE_WIN: None

}
strip = WS2812(spi_bus=1, led_count=LEDS_LEN)
strip_data = []
strip_progress_counters = dict()
strip_progress_data = []
lock = Pin("X1", Pin.OUT_OD)


def progress_inc(state):
    if state in strip_progress_counters:
        strip_progress_counters[state] += 1
    else:
        strip_progress_counters[state] = 1
    progress_data_recalc()


def progress_reset():
    strip_progress_counters.clear()
    progress_data_recalc()


def progress_data_recalc():
    strip_progress_data.clear()
    for state, progress in strip_progress_counters.items():
        leds = LEDSTATE[state].copy()
        ledp = LEDSTATE_PROG[state]
        if leds is None or ledp is None:
            continue
        take_leds = sum(ledp[:progress])
        leds.reverse()
        strip_progress_data.extend(leds[:take_leds])


def lock_locked():
    lock.value(1)


def lock_unlock():
    lock.value(0)


def strip_init():
    strip_data.clear()
    for i in range(LEDS_LEN):
        strip_data.append((0, 0, 0))
    strip.show(strip_data)


def strip_set_color(on_list=None, color=(0, 64, 0)):
    for i in range(LEDS_LEN):
        strip_data[i] = (0, 0, 0)
    for i in strip_progress_data:
        strip_data[i] = (0, 64, 0)

    if on_list is not None:
        for i in on_list:
            strip_data[i] = color
    strip.show(strip_data)


def strip_test():
    print("Test right")
    strip_set_color(LEDS_RIGHT)
    utime.sleep(1)
    print("Test left")

    strip_set_color(LEDS_LEFT)
    utime.sleep(1)
    print("Test up")

    strip_set_color(LEDS_TOP)
    utime.sleep(1)
    print("Test down")

    strip_set_color(LEDS_BOTTOM)
    utime.sleep(1)
    print("Test back")
    strip_set_color(LEDS_BACK)
    utime.sleep(1)
    strip_set_color()


RIGHT_SEQ = [STATE_LEFT, STATE_RIGHT, STATE_TOP, STATE_BOTTOM, STATE_BACK, STATE_TOP, STATE_BOTTOM, STATE_BACK,
             STATE_TOP, STATE_BOTTOM, STATE_BACK, STATE_LEFT, STATE_BOTTOM, STATE_RIGHT, STATE_BACK, STATE_TOP]


class State:
    def __init__(self):
        self.state = STATE_UNKNOWN
        self.laststate = STATE_UNKNOWN
        self.last_changed_at = utime.ticks_ms()
        self.stable_x = -1000
        self.stable_y = -1000
        self.stable_z = -1000
        self.seq_state = 0
        self.anim_start = None

    def __repr__(self):
        return STRSTATE[self.state]

    @staticmethod
    def _get_random_list(num=10):
        ret = list()
        for i in range(num):
            ret.append(rng() % LEDS_LEN)
        return ret

    def update(self):
        if self.state == STATE_WRONG:
            diff = utime.ticks_diff(utime.ticks_ms(), self.anim_start)
            if diff < 3000:
                return
            if diff < 6000:
                strip_set_color(self._get_random_list(round(LEDS_LEN / 2)), (rng() % 192, 0, 0))
                return
            strip_test()
            self.state = STATE_UNKNOWN

        if self.state == STATE_WIN:
            diff = utime.ticks_diff(utime.ticks_ms(), self.anim_start)
            if diff < 3000:
                return
            if diff < 6000:
                strip_set_color(self._get_random_list(round(LEDS_LEN / 2)), (0, rng() % 192, 0))
                return
            strip_test()
            self.state = STATE_UNKNOWN
            lock_locked()
            progress_reset()

        x, y, z = get_acc_normalized()
        if abs(x - self.stable_x) > SENSIVITY or abs(y - self.stable_y) > SENSIVITY or abs(
                z - self.stable_z) > SENSIVITY:
            self.state = STATE_MOVING
            self.last_changed_at = utime.ticks_ms()
            strip_set_color(self._get_random_list(), (rng() % 128, rng() % 128, rng() % 128))
            print("Moving")
            print(x, y, z)
            self.stable_x = x
            self.stable_y = y
            self.stable_z = z
            return
        diff = utime.ticks_diff(utime.ticks_ms(), self.last_changed_at)
        if diff < 3000:  # Ждем пока устоиться
            return

        if x > DIRECTION_SENS:
            self.state = STATE_RIGHT
        elif x < - DIRECTION_SENS:
            self.state = STATE_LEFT
        elif y > DIRECTION_SENS:
            self.state = STATE_BACK
        elif y < -DIRECTION_SENS:
            self.state = STATE_FRONT
        elif z > DIRECTION_SENS:
            self.state = STATE_TOP
        elif z < -DIRECTION_SENS:
            self.state = STATE_BOTTOM

        if self.state != self.laststate and self.state not in [STATE_UNKNOWN, STATE_MOVING]:
            self.laststate = self.state
            print("State changed to " + repr(self))
            if RIGHT_SEQ[self.seq_state] != self.state:
                print("Wrong state")
                strip_set_color(LEDSTATE[self.state], (255, 0, 0))
                self.state = STATE_WRONG
                self.seq_state = 0
                self.anim_start = utime.ticks_ms()
                lock_locked()
                progress_reset()
                return
            self.seq_state += 1
            strip_set_color(LEDSTATE[self.state])
            progress_inc(self.state)
            if self.seq_state == len(RIGHT_SEQ):
                print("WIN")
                lock_unlock()
                self.anim_start = utime.ticks_ms()
                self.seq_state = 0
                self.state = STATE_WIN


def get_acc_normalized():
    x = ACC.x()
    y = ACC.y()
    z = ACC.z()
    s = abs(x) + abs(y) + abs(z)
    nx = x / s
    ny = y / s
    nz = z / s
    return nx, ny, nz


def setup():
    UPDATES.append(State())


def main():
    strip_init()
    strip_test()
    lock_locked()
    while True:
        start_time = utime.ticks_ms()
        for u in UPDATES:
            u.update()

        end_time = utime.ticks_ms()
        diff = utime.ticks_diff(end_time, start_time)
        if diff < TARGET_TIME:
            utime.sleep_ms(TARGET_TIME - diff)


if __name__ == "__main__":
    setup()
    main()
